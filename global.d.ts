declare module '*.png'
declare module '*.gif'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.svg'
declare module '*.css'
declare module '*.less'
declare module '*.scss'
declare module '*.sass'
declare module '*.styl'
declare module '@tarojs/mobx'

declare const wx: any

/**
 * helper
 */
type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

type Overwrite<T, U> = Omit<T, Extract<keyof T, keyof U>> & U

type PartialWith<T, K extends keyof T> = Partial<Pick<T, K>> &
  Pick<T, Exclude<keyof T, K>>

type RequiredWith<T, K extends keyof T> = Required<Pick<T, K>> &
  Pick<T, Exclude<keyof T, K>>