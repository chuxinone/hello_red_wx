import { createApi, ListData, BaseData } from '../helper/api'

// models
import ProductsColumn from '../models/ProductsColumn'

/**
 * 获取栏目首页数据
 */
export namespace getList {
  export interface Param {
    page: number
    limit: number
  }
  export type Response = ListData<ProductsColumn>
}
export const getList = createApi<[getList.Param], getList.Response>(param => ({
  url: '/productsColumn/list',
  method: 'POST',
  data: param,
}))

/**
 * 获取栏目首页数据
 */
export namespace queryById {
  export interface Param {
    id: number
  }
  export type Response = BaseData<ProductsColumn>
}
export const queryById = createApi<[queryById.Param], queryById.Response>(param => ({
  url: `/productsColumn/get/${param.id}`,
  method: 'POST',
  data: param,
}))
