import { createApi, ListData } from '../helper/api'

// models
import Advertisement from '../models/Advertisement'

/**
 * 获取广告首页数据
 */
export namespace getList {
  export type Response = ListData<Advertisement>
}
export const getList = createApi<getList.Response>(() => ({
  url: '/advertisement/list',
  method: 'GET'
}))
