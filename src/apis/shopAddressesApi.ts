import { createApi, ListData } from '../helper/api'

// models
import OrderProduct from '../models/OrderProduct'
import ShopAddresses from '../models/ShopAddresses'

/**
 * 新增/修改地址
 */
export namespace addOrUpdate {
  export type Param = {
    id?: number
    uid: number // 用户id
    isDefault: number
    consignee: string // 收货人
    mobile: string
    zipCode?: string
    provinceName: string
    cityName: string
    districtnName: string
    streetName: string
    address: string
  }

  export type Response = ListData<OrderProduct>
}
export const addOrUpdate = createApi<[addOrUpdate.Param], addOrUpdate.Response>(param => ({
  url: '/shopAddresses/addOrUpdate',
  method: 'POST',
  data: param
}))

/**
 * 根据用户ID获取地址列表
 */
export namespace getAddrByUserId {
  export type Param = {
    userId: number // 用户id
    page?: number
    limit?: number
  }

  export type Response = ListData<ShopAddresses>
}
export const getAddrByUserId = createApi<[getAddrByUserId.Param], getAddrByUserId.Response>(param => ({
  url: `/shopAddresses/getAddrByUserId/${param.userId}`,
  method: 'POST',
  data: param
}))

/**
 * 删除地址
 */
export namespace remove {
  export type Param = {
    id: number
  }

  export type Response = ListData<ShopAddresses>
}
export const remove = createApi<[remove.Param], remove.Response>(param => ({
  url: '/shopAddresses/delete',
  method: 'POST',
  data: param
}))
