import { createApi, ListData, BaseData } from '../helper/api'

// models
import OrderProduct from '../models/OrderProduct'

/**
 * 根据分类ID查询商品数据
 */
export namespace getList {
  export type Param = {
    status?: number[]
    page?: number
    limit?: number
  }

  export type Response = ListData<OrderProduct>
}
export const getList = createApi<[getList.Param], getList.Response>(param => ({
  url: '/order/get',
  method: 'POST',
  data: param
}))


/**
 * 订单生成
 */
export namespace addOrder {
  export type Param = {
    productId: number
    userId: number
    num: number
    totalPrices: number // TODO: 不应该前端传给后端，待接口修改再处理
  }

  export type Response = BaseData<OrderProduct>
}
export const addOrder = createApi<[addOrder.Param], addOrder.Response>(param => ({
  url: '/order/addOrder',
  method: 'POST',
  data: param
}))