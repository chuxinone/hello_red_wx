import { createApi, BaseData } from '../helper/api'

// models
import UserAuth from '../models/UserAuth'

/**
 * 根据分类ID查询商品数据
 */
export namespace login {
  export type Param = {
    code: string
  }

  export type Response = BaseData<UserAuth>
}
export const login = createApi<[login.Param], login.Response>(param => ({
  url: '/wx/login',
  method: 'POST',
  data: param,
  notLogin: true
}))
