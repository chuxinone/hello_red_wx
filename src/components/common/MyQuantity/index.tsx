import { View, Image, Text } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

import './index.scss'

export interface DataProps {
  value: number
}
export interface EventProps {
  onChange?: (value: number) => void
}
export type Props = DataProps & EventProps

  interface State {}

  class MyQuantity extends Component<Props, State> {
    // ---------------- 私有属性 ----------------

    // ---------------- 生命周期 ----------------

    // ---------------- 计算属性 ----------------

    handleChange = (value: number) => {
      if (this.props.onChange) {
        this.props.onChange(value)
      }
    }

    render() {
      const { value } = this.props
      const vals = (value + '').split('')
      return (
        <View className='my-quantity'>
          <Image
            className='my-quantity__inc'
            src={require('../../../assets/icons/increase.jpg')}
            onClick={this.handleChange.bind(this, value + 1)}
          />
          <View className='my-quantity__value'>
            {vals.map((val, i) => (
              <Text key={`${i}-${val}`}>{val}</Text>
            ))}
          </View>
          <Image
            className='my-quantity__dec'
            src={require('../../../assets/icons/decrease.jpg')}
            onClick={this.handleChange.bind(this, value - 1)}
          />
        </View>
      )
    }
  }

  export default MyQuantity
