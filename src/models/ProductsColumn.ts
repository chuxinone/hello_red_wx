export default interface ProductsColumn {
  id: number

  title: string

  ico: string

  describe: string
}
