export default interface UserAuth {
  id: number
  /**用户ID*/
  uid: number
  /**登录类型 1微信 2微博 3手机号 4邮箱 5用户名*/
  identityType: number

  /**标识标识（手机号 邮箱 用户名或第三方应用的唯一标识）*/
  identifier: string
  /**密码凭证（站内的保存密码，站外的不保存或保存TOKEN）*/
  credential: string
}
