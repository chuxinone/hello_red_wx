export default interface OrderProductItems {
  id: number
  /**订单ID*/
  oid: number
  /**商品ID*/
  pid: number
  /**购买数量*/
  buyCount: number
}
