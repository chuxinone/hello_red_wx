export default interface ImageInfo {
  id: number
  pid: number
  imgType: number
  imgUrl: string
  imgDesc: string
  displayOrder: number
}
