namespace Advertisement {
  export enum TargetType {
    /**
     * 商品
     */
    Product = 0,
    /**
     * 店铺
     */
    Store = 1,
  }

  export const TargetTypeText = {
    [TargetType.Product]: '商品',
    [TargetType.Store]: '店铺',
  }
}
interface Advertisement {
  id: number

  title: string

  status: number

  img: string

  targetType: Advertisement.TargetType

  targetId: number

  createTime: number
}

export default Advertisement
