/**
 * 商品类别
 */
export default interface ProductsCategory {
  id: number
  /**上级类别ID*/
  topID: number
  /**类别名称*/
  categoryName: string
  /**0 目录 1节点*/
  isParent: number
  /**是否作废 0 否 1 是*/
  cStatus: number

  name: string
  children: ProductsCategory[]
}
