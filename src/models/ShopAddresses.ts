export default interface ShopAddresses {
  id: number
  /**用户ID*/
  uid: number
  /**是否为默认地址*/
  isDefault: number
  /**收货人*/
  consignee: string
  /**手机号*/
  mobile: string
  /**邮政编码*/
  zipCode: string
  /**省*/
  provinceName: string
  /**市*/
  cityName: string
  /**区*/
  districtnName: string
  /**街道*/
  streetName: string
  /**详细地址*/
  address: string
}
