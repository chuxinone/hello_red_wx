import Taro from '@tarojs/taro'

const designWidth: number = process.env.DESIGN_WIDTH as any
const systemInfo = Taro.getSystemInfoSync()
const screenWidth = systemInfo.screenWidth

/* eslint-disable-next-line */
export function calcPx(designPx: number) {
  return (screenWidth * designPx) / designWidth
}
