import Taro from '@tarojs/taro'
import { action, observable } from 'mobx'

// helper
import * as request from '../helper/request'

// config
import config from '../config'

// apis
import * as wxApi from '../apis/wxApi'

// models
import UserAuth from '../models/UserAuth'

export class AccountStore {
  waitLoginPromises: Array<{
    resolve: (...args: any[]) => any
    reject: (...args: any[]) => any
  }> = []
  /**
   * 获取当前用户
   */
  waitLoadUserPromises: Array<{
    resolve: (...args: any[]) => any
    reject: (...args: any[]) => any
  }> = []

  @observable
  userAuth: UserAuth | null = null

  /**
   * login
   */
  @action
  async login() {
    // 开始登录
    const { code } = await Taro.login()
    const resp = await wxApi.login({
      code,
    })

    // 登录成功
    this.userAuth = resp.data

    // 设置request的loginSesion用于签名
    request.loginSuccess(resp.data)

    // 处理等待登录的
    while (this.waitLoginPromises.length) {
      const promise = this.waitLoginPromises.shift()
      if (promise && promise.resolve) {
        try {
          promise.resolve(this.userAuth)
        } catch (error) {
          console.error(error)
        }
      }
    }
  }

  @action
  async getLoginSession() {
    // 存在直接返回
    if (this.userAuth) {
      return this.userAuth
    }

    // 加载中，等待加载完成
    return new Promise<UserAuth>((resolve, reject) => {
      this.waitLoginPromises.push({
        resolve,
        reject,
      })
    })
  }

  // @action
  // async loadCurrentUserInfo(
  //   param: {
  //     refresh?: boolean // 是否强制刷新，false 时：已加载、加载中不会重新加载
  //   } = {},
  // ) {
  //   // 不强制刷新
  //   if (!param.refresh) {
  //     // 存在直接返回
  //     if (this.user && this.brandBusiness && this.healthUserInfo) {
  //       return {
  //         user: this.user,
  //         brandBusiness: this.brandBusiness,
  //         healthUserInfo: this.healthUserInfo,
  //       }
  //     }

  //     // 加载中，等待加载完成
  //     if (this.loading) {
  //       return new Promise<wxApi.GetCurrentUserInfoResponse>((resolve, reject) => {
  //         this.waitLoadUserPromises.push({
  //           resolve,
  //           reject,
  //         })
  //       })
  //     }
  //   }

  //   // 开始加载
  //   this.loading = true

  //   const resp = await wxApi.getCurrentUserInfo()

  //   this.user = resp.user
  //   this.brandBusiness = resp.brandBusiness
  //   this.healthUserInfo = resp.healthUserInfo
  //   // 加载完成
  //   this.loading = false

  //   setTimeout(() => {
  //     // 处理等待加载的
  //     while (this.waitLoadUserPromises.length) {
  //       const promise = this.waitLoadUserPromises.shift()
  //       if (promise && promise.resolve) {
  //         try {
  //           promise.resolve()
  //         } catch (error) {
  //           console.error(error)
  //         }
  //       }
  //     }
  //   })

  //   return {
  //     user: this.user,
  //     brandBusiness: this.brandBusiness,
  //     healthUserInfo: this.healthUserInfo,
  //   }
  // }
}

export default new AccountStore()
