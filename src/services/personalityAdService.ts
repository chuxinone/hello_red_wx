import PersonalityAd from '../models/PersonalityAd'

import productDetailStore from '../pages/productDetail/store'

export function navigateToTarget(personalityAd: PersonalityAd): void {
  if (personalityAd.targetType === PersonalityAd.TargetType.Product) {
    productDetailStore.navigateTo({ productId: personalityAd.targetId })
  }
}
