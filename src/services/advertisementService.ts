import Advertisement from '../models/Advertisement'

import productDetailStore from '../pages/productDetail/store'

export function navigateToTarget(advertisement: Advertisement): void {
  if (advertisement.targetType === Advertisement.TargetType.Product) {
    productDetailStore.navigateTo({ productId: advertisement.targetId })
  }
}
