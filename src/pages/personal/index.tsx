import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAvatar, AtList, AtListItem } from 'taro-ui-hello-red'

import { observer, inject } from '@tarojs/mobx'

// components
import OrderItem from './components/OrderItem'

import './index.scss'

type DataProps = {}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return {}
})
@observer
class PersonalPage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '个人',
    backgroundColor: '#F3F2ED'
  }

  componentWillReceiveProps(nextProps: Props) {
    console.info(this.props, nextProps)
  }

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  navigateToOrder = (tabIndex?: number) => {
    Taro.navigateTo({
      url: `/pages/orderList/index?tabIndex=${tabIndex}`
    })
  }
  navigateToAddress = () => {
    Taro.navigateTo({
      url: '/pages/address/list/index'
    })
  }
  render() {
    return (
      <View className='personal-page'>
        {/* 用户 */}
        <View className='personal-page__user'>
          <AtAvatar className='personal-page__user-avatar' circle image='' />
          <Text className='personal-page__user-name'>fdsfsd</Text>
        </View>
        {/* 我的订单 */}
        <View className='personal-page__orders'>
          <View className='personal-page__orders-title'>
            <Text className='personal-page__orders-title-one'>我的订单</Text>
            <Text className='personal-page__orders-title-two' onClick={this.navigateToOrder.bind(this, 0)}>
              全部订单
            </Text>
          </View>
          <View className='personal-page__orders-content'>
            <OrderItem icon={require('./assets/order/pending-pay.png')} label='待付款' onClick={this.navigateToOrder.bind(this, 1)} />
            <OrderItem icon={require('./assets/order/pending-delivery.png')} label='待发货' onClick={this.navigateToOrder.bind(this, 2)} />
            <OrderItem icon={require('./assets/order/received.png')} label='已收货' onClick={this.navigateToOrder.bind(this, 3)} />
            <OrderItem icon={require('./assets/order/completed.png')} label='已完成' onClick={this.navigateToOrder.bind(this, 4)} />
          </View>
        </View>
        {/* 列表 */}
        <View className='personal-page__list'>
          <AtList>
            <AtListItem title='我的地址' arrow='right' onClick={this.navigateToAddress} />
            <AtListItem title='意见反馈' arrow='right' />
            <AtListItem title='分享红品' arrow='right' />
          </AtList>
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default PersonalPage
