import Taro, { Component, Config } from '@tarojs/taro'
import { Button, View, Swiper, SwiperItem, Image, Text } from '@tarojs/components'

import { observer, inject } from '@tarojs/mobx'

import './index.scss'

// components
import AuthView from '../../components/common/AuthView'

// models
import Advertisement from '../../models/Advertisement'
import ProductsColumn from '../../models/ProductsColumn'
import PersonalityAd from '../../models/PersonalityAd'

// services
import * as advertisementService from '../../services/advertisementService'
import * as personalityAdService from '../../services/personalityAdService'

// store
import store from './store'
import productListStore from '../productList/store'

type DataProps = {
  homeStore: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { homeStore: store }
})
@observer
class HomePage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '首页',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentDidMount() {
    Taro.showLoading({ title: '加载中' })
    await this.props.homeStore.init()
    Taro.hideLoading()
  }

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  /**
   * 点击广告
   */
  handleClickAdvertisement = (advertisement: Advertisement) => {
    advertisementService.navigateToTarget(advertisement)
  }

  /**
   * 点击卡片
   */
  handleClickCard = (productsColumn: ProductsColumn) => {
    productListStore.navigateTo({ productsColumnId: productsColumn.id })
  }

  /**
   * 点击推荐
   */
  handleClickRecommend = (personalityAd: PersonalityAd) => {
    personalityAdService.navigateToTarget(personalityAd)
  }

  render() {
    const { homeStore } = this.props
    const { advertisementList, productsColumnList, personalityList } = homeStore
    return (
      <AuthView>
        <View className='home-page'>
          {/* 顶部轮播图 */}
          <Swiper className='home-page__top-imgs' indicatorDots autoplay circular>
            {advertisementList.map(item => {
              return (
                <SwiperItem key={item.id} onClick={this.handleClickAdvertisement.bind(this, item)}>
                  <Image className='home-page__top-imgs-item' src={item.img} />
                </SwiperItem>
              )
            })}
          </Swiper>

          {/* 一条线 */}
          <View className='home-page__top-line' />

          {/* 卡片 */}
          <View className='home-page__cards'>
            {productsColumnList.map(card => {
              return (
                <View key={card.id} className='home-page__cards-item' onClick={this.handleClickCard.bind(this, card)}>
                  <View className='home-page__cards-item-icon'>
                    <Image src={card.ico} />
                  </View>
                  <Text className='home-page__cards-item-label'>{card.title}</Text>
                  <Text className='home-page__cards-item-sublabel'>{card.describe}</Text>
                </View>
              )
            })}
          </View>

          {/* 推荐 */}
          <View className='home-page__recommend'>
            <View className='home-page__recommend-title'>今日推荐</View>
            {personalityList.map(personality => {
              return (
                <View
                  key={personality.id}
                  className='home-page__recommend-item'
                  onClick={this.handleClickRecommend.bind(this, personality)}
                >
                  <Image className='home-page__recommend-item-picture' src={personality.img} />
                  {/* <View className='home-page__recommend-item-label'>
                    <View className='home-page__recommend-item-label-title'>
                      <View className='home-page__recommend-item-label-title-first'>{recommend.title}</View>
                      <View className='home-page__recommend-item-label-title-second'>english</View>
                    </View>

                    <View className='home-page__recommend-item-label-button'>
                      <Button>立即下单</Button>
                    </View>
                  </View> */}
                </View>
              )
            })}
          </View>
        </View>
      </AuthView>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default HomePage
