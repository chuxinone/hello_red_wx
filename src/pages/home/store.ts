import Taro from '@tarojs/taro'
import { action, observable } from 'mobx'

// apis
import * as advertisementApi from '../../apis/advertisementApi'
import * as personalityAdApi from '../../apis/personalityAdApi'
import * as productsColumnApi from '../../apis/productsColumnApi'

// models
import Advertisement from '../../models/Advertisement'
import ProductsColumn from '../../models/ProductsColumn'
import PersonalityAd from '../../models/PersonalityAd'

export interface CardItem {
  icon: string
  label: string
  subLabel: string
}

class HomeStore {
  @observable
  advertisementList: Advertisement[] = []

  @observable
  productsColumnList: Array<ProductsColumn> = []

  @observable
  personalityList: PersonalityAd[] = []

  @action
  async init() {
    Taro.showLoading({ title: '加载中...' })
    await new Promise(resolve => setTimeout(resolve, 500))

    await Promise.all([
      // 广告
      this.loadAdvertisementList(),
      // 推荐
      this.loadPersonalityAdList(),
      // 栏目
      this.loaDproductsColumnList()
    ])

    Taro.hideLoading()
  }

  /**
   * 广告
   */
  @action
  async loadAdvertisementList() {
    const resp = await advertisementApi.getList()

    this.advertisementList = resp.data
  }

  /**
   * 推荐
   */
  @action
  async loadPersonalityAdList() {
    const resp = await personalityAdApi.getList()

    this.personalityList = resp.data
  }

  /**
   * 栏目
   */
  @action
  async loaDproductsColumnList() {
    const resp = await productsColumnApi.getList({ page: 1, limit: 3 })

    this.productsColumnList = resp.data
  }
}

export default new HomeStore()
