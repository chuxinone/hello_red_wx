import { View, Input, Textarea } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

import './index.scss'

export interface DataProps {
  type: 'input' | 'textarea' | 'custom'
  label: string
  value?: string
}
export interface EventProps {
  onInput?: (value: string) => void
}
export type Props = DataProps & EventProps

interface State {}

class InputItem extends Component<Props, State> {
  // ---------------- 私有属性 ----------------

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {}
  }

  // ---------------- 生命周期 ----------------
  componentDidMount() {
    //
  }

  // ---------------- 计算属性 ----------------

  // ---------------- 方法 ----------------
  handleInput = (event: any) => {
    if (this.props.onInput) {
      this.props.onInput(event.detail.value)
    }
  }

  render() {
    const { value, label, type } = this.props
    return (
      <View className='input-item'>
        <View className='input-item__label'>{label}</View>
        <View className='input-item__constant'>
          {type === 'input' ? (
            <Input className='input-item__input' value={value} onInput={this.handleInput} />
          ) : type === 'textarea' ? (
            <Textarea className='input-item__textarea' value={value || ''} autoHeight maxlength={200} onInput={this.handleInput} />
          ) : (
            this.props.children
          )}
        </View>
      </View>
    )
  }
}

export default InputItem
