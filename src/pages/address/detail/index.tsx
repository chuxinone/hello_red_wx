import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Picker } from '@tarojs/components'
import { AtButton } from 'taro-ui-hello-red'

import { observer, inject } from '@tarojs/mobx'

import './index.scss'

// components
import MyRadio from '../../../components/common/MyRadio'
import InputItem from './components/InputItem'

// store
import store from './store'

type DataProps = {
  addressAddStore: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { addressAddStore: store }
})
@observer
class AddressAddPage extends Component<Props, State> {
  changeName = store.changeName
  changeMobile = store.changeMobile
  changeCity = store.changeRegion
  changeStreet = store.changeStreet
  changeAddress = store.changeAddress
  changeIsDefault = store.toggleIsDefault
  save = store.save
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    // navigationBarTitleText: '新增地址'
  }

  constructor(props: Props) {
    super(props)
  }

  // async componentWillPreload() {}

  handleChangeCity = (e: any) => {
    const values = e.detail.value as string[]
    store.changeRegion(values)
  }

  render() {
    const { name, mobile, region, address, isDefault } = this.props.addressAddStore
    return (
      <View className='address-detail-page'>
        <InputItem type='input' label='姓名' value={name} onInput={this.changeName} />
        <InputItem type='input' label='联系电话' value={mobile} onInput={this.changeMobile} />

        <Picker mode='region' value={region} onChange={this.handleChangeCity} customItem=''>
          <InputItem type='custom' label='城市地区'>
            {region.join(' ')}
          </InputItem>
        </Picker>
        {/* <InputItem type='textarea' label='街道' value={street} onInput={this.changeStreet} /> */}
        <InputItem type='textarea' label='详细地址' value={address} onInput={this.changeAddress} />

        <View className='address-detail-page__default' onClick={this.changeIsDefault}>
          <MyRadio checked={!!isDefault} />
          <Text className='address-detail-page__default-text'>设为默认地址</Text>
        </View>

        <AtButton type='primary' onClick={this.save}>
          保存
        </AtButton>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default AddressAddPage
