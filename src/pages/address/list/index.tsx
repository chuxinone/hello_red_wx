import Taro, { Component, Config } from '@tarojs/taro'

import { observer, inject } from '@tarojs/mobx'

import { View, Image, Text } from '@tarojs/components'

import { AtIcon } from 'taro-ui-hello-red'

import './index.scss'

import addressListStore from './store'

type DataProps = {
  addressListStore: typeof addressListStore
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { addressListStore: addressListStore }
})
@observer
class AddressListPage extends Component<Props, State> {
  navigateToAdd = addressListStore.navigateToAdd
  navigateToDetail = addressListStore.navigateToDetail
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '我的地址',
  }

  select = addressListStore.select

  constructor(props: Props) {
    super(props)
  }

  async componentWillPreload(params: any) {
    console.info(params)
    Taro.showLoading({ title: '加载中' })
    await this.props.addressListStore.init({ selectMode: !!params.selectMode })
    Taro.hideLoading()
  }

  render() {
    const { addressList, selectedId, selectMode } = this.props.addressListStore
    return (
      <View className='address-list-page'>
        <View className='address-list-page__address-list'>
          {addressList.map(address => {
            return (
              <View
                key={address.id}
                className='address-list-page__address'
                onClick={this.select.bind(this, address.id)}
              >
                {selectMode && (
                  <View className='address-list-page__address-check'>{selectedId === address.id && <AtIcon value='check' size='25' color='#ff6161' />}</View>
                )}
                <View className='address-list-page__address-left'>
                  <Text className='address-list-page__address-name'>{address.consignee}</Text>
                  {address.isDefault && <View className='address-list-page__address-default'>默认</View>}
                </View>
                <View className='address-list-page__address-center'>
                  <View>
                    <Text className='address-list-page__address-mobile'>{address.mobile}</Text>
                  </View>
                  <Text className='address-list-page__address-address'>
                    {address.provinceName}
                    {address.cityName}
                    {address.districtnName}
                    {address.streetName}
                    {address.address}
                  </Text>
                </View>
                <View className='address-list-page__address-right' onClick={this.navigateToDetail.bind(this, address)}>
                  <Image className='address-list-page__address-edit' src={require('../../../assets/icons/edit.jpg')} />
                </View>
              </View>
            )
          })}
        </View>

        <View className='address-list-page__add' onClick={this.navigateToAdd}>
          添加新地址
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default AddressListPage
