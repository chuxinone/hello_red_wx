import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'

import { observer, inject } from '@tarojs/mobx'

import toNumber from 'lodash/toNumber'

// components
import { AtTextarea, AtCheckbox, AtButton } from 'taro-ui-hello-red'

// styles
import './index.scss'

// store
import payStore from './store'

type DataProps = {
  payStore: typeof payStore
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { payStore }
})
@observer
class PayPage extends Component<Props, State> {
  handleCheckboxChange = payStore.handleCheckboxChange
  handlePayCheckboxChange = payStore.handlePayCheckboxChange
  submitPay = payStore.submitPay
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '确认支付',
    navigationBarBackgroundColor: '#F2F2F2',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentWillPreload() {

    await this.props.payStore.init()
  }

  handleChangeRemark = (event: any) => {
    payStore.changeRemark(event.detail.value)
  }

  render() {
    const { remark, checkboxOptions, currentCheckedList, payCheckboxOptions, payCheckedList } = this.props.payStore

    return (
      <View className='pay-page'>
        {/* 备注 */}
        <View className='pay-page__remark'>
          <View className='pay-page__remark-label'>备注</View>
          <View className='pay-page__remark-textarea'>
            <AtTextarea value={remark} onChange={this.handleChangeRemark} />
          </View>
        </View>

        {/* 收货地址 */}
        <View className='pay-page__way'>
          <AtCheckbox
            options={checkboxOptions}
            selectedList={currentCheckedList}
            onChange={this.handleCheckboxChange}
          />
        </View>

        {/* 支付方式 */}
        <View className='pay-page__pay'>
          <View className='pay-page__pay-tip'>注意：外送需要加收2元配送费</View>
          <AtCheckbox
            options={payCheckboxOptions}
            selectedList={payCheckedList}
            onChange={this.handlePayCheckboxChange}
          />
        </View>

        {/* 立即支付 */}
        <View className='pay-page__button'>
          <AtButton type='primary' onClick={this.submitPay}>立即支付</AtButton>
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default PayPage
