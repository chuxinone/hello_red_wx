import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// models
import OrderProduct from '../../models/OrderProduct'

class OrderListStore {
  tabList = [
    { title: '全部', status: [] },
    { title: '待付款', status: [OrderProduct.Status.PendingPay] },
    { title: '待发货', status: [OrderProduct.Status.PendingDelivery] },
    { title: '已发货', status: [OrderProduct.Status.PendingReceived, OrderProduct.Status.PendingComment] },
    { title: '已完成', status: [OrderProduct.Status.TransactionSuccess, OrderProduct.Status.Refund] }
  ]

  @observable
  currentTab: number = 0

  @observable
  orderList: OrderProduct[] = []

  @computed.struct
  get currentStatus() {
    return this.tabList[this.currentTab].status
  }

  @action
  async init(tabIndex: number) {
    this.currentTab = tabIndex
    this.loadList()
  }

  @action
  tabChange = (value: number) => {
    this.currentTab = value
    this.loadList()
  }

  @action
  async loadList() {
    Taro.showLoading({ title: '加载中...' })

    const resp = await orderApi.getList({ status: this.currentStatus, page: 1, limit: 20 })
    this.orderList = resp.data
    Taro.hideLoading()
  }
}

export default new OrderListStore()
