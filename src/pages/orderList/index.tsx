import Taro, { Component, Config } from '@tarojs/taro'
import { View, ScrollView, Text, Image } from '@tarojs/components'
import { AtTabs } from 'taro-ui-hello-red'

import { observer, inject } from '@tarojs/mobx'

import isNumber from 'lodash/isNumber'

import './index.scss'

import store from './store'

type DataProps = {
  store: typeof store
}

type EventProps = {}

type State = {
  // currentTab: number
}

type Props = DataProps & EventProps

@inject(() => {
  return { store }
})
@observer
class OrderListPage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '我的订单',
    backgroundColor: '#F3F2ED'
  }

  tabChange = store.tabChange

  constructor(props: Props) {
    super(props)
  }

  componentDidMount() {}

  async componentWillPreload(params: any) {
    let tabIndex = parseFloat(params.tabIndex)

    if (!isNumber(tabIndex)) {
      tabIndex = 0
    }
    store.init(tabIndex)
  }

  render() {
    const { orderList, tabList, currentTab } = this.props.store

    return (
      <View className='order-list-page'>
        <AtTabs current={currentTab} tabList={tabList} onClick={this.tabChange}>
          {/* {tabList.map((_, i) => {
            return <AtTabsPane key={i} current={currentTab} index={i} />
          })} */}
        </AtTabs>

        <ScrollView className='order-list-page__tabs-pane' scrollY>
          {orderList.map(order => {
            return (
              <View className='order-item' key={order.id}>
                <View className='order-item__top'>
                  <Text className='order-item__no'>订单编号：{order.id}</Text>
                  <Text className='order-item__status'>{order.oStatus}</Text>
                </View>
                <View className='order-item__products'>
                  {order.orderProductItems.map(orderProduct => {
                    return (
                      <View key={orderProduct.id} className='order-item__products-item'>
                        <Image className='order-item__products-item-photo' src='https://free.com.tw/blog/wp-content/uploads/2014/08/Placekitten480-g.jpg' />

                        <View className='order-item__products-item-right'>
                          <View className='order-item__products-item-name'>
                            <Text>商品名称</Text>
                            <Text className='order-item__products-item-quantity'>x{orderProduct.buyCount}</Text>
                          </View>
                          <View className='order-item__products-item-desc'>商品规格</View>
                          <View className='order-item__products-item-price'>¥100.04</View>
                        </View>
                      </View>
                    )
                  })}
                </View>
                <View className='order-item__bottom'>
                  <View className='order-item__bottom-left'>
                    <Text className='order-item__total-quantity'>共{order.orderProductItems.length}件商品 需付款:</Text>
                    <Text className='order-item__total-price-unit'>¥</Text>
                    <Text className='order-item__total-price'>{order.totalPrices}</Text>
                  </View>

                  <View className='order-item__buttons'>
                    <View className='order-item__secondary-button'>取消订单</View>
                    <View className='order-item__primary-button'>再次购买</View>
                  </View>
                </View>
              </View>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default OrderListPage
