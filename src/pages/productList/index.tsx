import Taro, { Component, Config } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text } from '@tarojs/components'

import { observer, inject } from '@tarojs/mobx'
import { AtActivityIndicator } from 'taro-ui-hello-red'
import toNumber from 'lodash/toNumber'

import './index.scss'

// components
import Categorys from './components/Categorys'

// store
import store from './store'
import productDetailStore from '../productDetail/store'

type DataProps = {
  productListStore: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { productListStore: store }
})
@observer
class ProductListPage extends Component<Props, State> {
  changeCategory = store.changeCategory
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    backgroundColor: '#F3F2ED',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentWillPreload(params: any) {
    const productsColumnId = toNumber(params.productsColumnId)
    await store.init({ productsColumnId })
  }

  // onPageScroll = (...args: any[]) => {
  //   console.log(...args)
  // }

  handleClickPersonal = () => {
    Taro.switchTab({
      url: '/pages/personal/index',
    })
  }

  navigateToProduct = (productId: number) => {
    productDetailStore.navigateTo({ productId })
  }

  render() {
    const { productListStore } = this.props
    const { isInit, isLoadingProducts, topImgs, products, productCategorys, currentCategoryId } = productListStore
    return (
      <View className='product-list-page'>
        {/* 顶部轮播图 */}
        <Swiper
          className='product-list-page__top-imgs'
          indicatorColor='#FFFFFF'
          indicatorActiveColor='#FF6161'
          indicatorDots
          autoplay
          circular
        >
          {topImgs.map((item, i) => {
            return (
              <SwiperItem key={i}>
                <Image className='product-list-page__top-imgs-item' src={item} />
              </SwiperItem>
            )
          })}
        </Swiper>

        {/* 中间 */}
        <View className='product-list-page__mid'>
          {/* 地址 */}
          <View className='product-list-page__store'>
            <View className='product-list-page__store-name'>广州麦当劳花城大道第四分店</View>
            <View className='product-list-page__store-address'>
              广东省广州市天河区珠江新城花城大道88号首层119A、120A、121A
            </View>
          </View>
          {/* 个人中心 */}
          <View className='product-list-page__personal' onClick={this.handleClickPersonal}>
            <Image
              className='product-list-page__personal-icon'
              src={require('../../assets/navs/personal-selected.png')}
            />
            <Text className='product-list-page__personal-label'>个人中心</Text>
          </View>
        </View>

        {/* 初始化前 */}
        {!isInit && (
          <View style={{ display: 'flex', justifyContent: 'center' }}>
            <AtActivityIndicator />
          </View>
        )}

        {/* 没有商品分类 */}
        {isInit && (!productCategorys || !productCategorys.length) && (
          <View style={{ width: '100%', textAlign: 'center' }}>无商品分类</View>
        )}

        {/* 有商品分类 */}
        {isInit && productCategorys && productCategorys.length && (
          <View className='product-list-page__content'>
            <View className='product-list-page__content-left'>
              {/* 搜索 */}
              <View className='product-list-page__content-search'>
                <Image src={require('./assets/search.png')} />
                <Text>搜索</Text>
              </View>
              {/* 类别 */}
              <View className='product-list-page__content-category'>
                <Categorys list={productCategorys} currentId={currentCategoryId} onClick={this.changeCategory} />
              </View>
            </View>

            {/* 商品列表 */}
            <View className='product-list-page__content-list'>
              {isLoadingProducts && (
                <View className='product-list-page__content-list-loading'>
                  <AtActivityIndicator />
                </View>
              )}
              {products.map(product => {
                return (
                  <View key={product.id} className='product-list-page__content-list-item'>
                    <Image
                      className='product-list-page__content-list-item-pic'
                      src='https://free.com.tw/blog/wp-content/uploads/2014/08/Placekitten480-g.jpg'
                    />
                    <View className='product-list-page__content-list-item-right'>
                      <View className='product-list-page__content-list-item-name'>{product.title}</View>
                      <View className='product-list-page__content-list-item-desc'>{product.synopsis}</View>
                      <View className='product-list-page__content-list-item-price'>￥{product.shoppPrice}</View>
                      <View
                        className='product-list-page__content-list-item-button'
                        onClick={this.navigateToProduct.bind(this, product.id)}
                      >
                        立即购买
                      </View>
                    </View>
                  </View>
                )
              })}
            </View>
          </View>
        )}
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default ProductListPage
