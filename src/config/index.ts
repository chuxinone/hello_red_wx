import Taro from '@tarojs/taro'

import { getConfigByAppId } from './config'

const accountInfo = wx.getAccountInfoSync()
const appId = accountInfo.miniProgram.appId

const config = getConfigByAppId(appId)

/* 第三方平台配置 */
if (Taro.getExtConfigSync) {
  const extConfig: any = Taro.getExtConfigSync()
  if (extConfig) {
    Object.keys(extConfig).forEach(key => {
      config[key] = extConfig[key]
    })
  }
}

export default config
