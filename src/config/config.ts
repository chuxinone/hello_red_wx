export interface Config {
  online: boolean
  apiUrl: string
  appId: string
}

export const defaultConfig: Config = {
  online: false,
  apiUrl: '',
  appId: ''
}

export const allConfig: Config[] = [
  {
    appId: 'wx7f2b6c6233b463c6',
    online: false,
    apiUrl: 'https://127.0.0.1:8443'
  },

]

export function getConfigByAppId(appId: string) {
  const config = allConfig.find(item => item.appId === appId)

  // 没有匹配相应配置时，使用第一个配置
  if (!config) {
    return {
      ...defaultConfig,
      appId
    }
    // throw new Error(
    //   `没有对应 appId = ${appId} 的配置，请在 src/config/config.ts 中配置`
    // )
  }
  return config
}
