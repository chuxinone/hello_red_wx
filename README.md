## 框架

- `typescirpt`
- `Taro` 基础框架
- `taro-ui` 基于 Taro 的 UI 库
- `dayjs` 日期时间
- `classnames` 样式
- `mobx` 状态管理

## 开发

``` bash
# 安装依赖
yarn
# or
yarn install 

# 运行开发模式
yarn dev:weapp
```

## 项目结构

```bash
├── build
│   └── ... # 构建脚本、配置等
├── config
│   └── ... # 构建参数，环境变量
├── dist
│   └── ... # 构建之后的文件，用于发布
├── src
│   ├── apis
│   │   └── ... # API请求
│   ├── assets
│   │   └── ... # 资源文件
│   ├── components
│   │   ├── common
│   │   │   └── ... # 公共组件
│   │   ├── moduleA
│   │   │   └── ... # 模块A相关组件
│   │   └── ... # 其他组件
│   ├── constant
│   │   └── ... # 常量
│   ├── helper
│   │   └── ... # 工具
│   ├── libs
│   │   └── ... # 第三方包
│   ├── model
│   │   ├── enums
│   │   │   └── ... # 枚举
│   │   └── ... # 数据模型
│   ├── pages ... # 页面
│   │   ├── pageA
│   │   │   ├── assets
│   │   │   │   ├── ... # 页面相关资源文件
│   │   │   ├── components
│   │   │   │   └── ... # 页面相关组件
│   │   │   └── ... # 页面
│   │   └── pageB
│   │       └── ... # 页面
│   ├── services
│   │   └── ... # 相关业务代码
│   ├── store
│   │   └── ... # store（mobx）
│   ├── styles
│   │   └── ... # 全局样式
│   ├── views
│   │   └── ... # 页面
│   └── app.tsx             # 顶级组件
└── types
    └── ... # typescript 全局类型
```

##  组件规范

- 属性

- constructor

- 生命周期

- ‘计算属性’

- 方法

- render

```tsx
import { View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

export interface DataProps {
  fistName: string
  lastName: string
}
export interface EventProps {
  onHello: () => void
}
export type Props = DataProps & EventProps

interface State {
  isShow: boolean
}

class Template extends Component<Props, State> {
  // ---------------- 私有属性 ----------------
  /**
   * x
   */
  x: number

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {
      isShow: false
    }
  }

  // ---------------- 生命周期 ----------------
  componentDidMount() {
    // TODO
  }

  // ---------------- 计算属性 ----------------
  /**
   * full name
   */
  fullName = () => {
    return this.props.fistName + this.props.lastName
  }

  // ---------------- 方法 ----------------
  /**
   * hello
   */
  hello = () => {
    this.props.onHello()
  }

  render() {
    return (
      <View>
        <View>{this.fullName()}</View>
        <View>{this.props.children}</View>
      </View>
    )
  }
}

export default Template
```

### 第三方组件报错问题
`types/global.d.ts`
``` ts
/**
 *  第三方组件
 */
declare namespace JSX {
  interface IntrinsicElements {
    'ff-canvas': any
  }
}
```